package fr.devid.demoapplication

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class Fragment3ViewModel : ViewModel() {

    private val _basicClassesLiveData = MutableLiveData<List<BasicClass>>()
    val basicClassesLiveData: LiveData<List<BasicClass>> = _basicClassesLiveData

    init {
        _basicClassesLiveData.value = (1..100).map { BasicClass(it) }
    }

    fun removeBasicClass(basicClassToRemove: BasicClass) {
        val basicClasses = _basicClassesLiveData.value ?: return

//        basicClasses.filter { basicClass ->
//            return@filter basicClass != basicClassToRemove
//        }
        _basicClassesLiveData.value = basicClasses.filter { it != basicClassToRemove }
    }

    fun addBasicClass() {
        val basicClasses = _basicClassesLiveData.value ?: return
//        _basicClassesLiveData.value = basicClasses.plus(newBasicClass)
        _basicClassesLiveData.value = basicClasses + BasicClass(999)
    }

    fun updateBasicClass(oldBasicClass: BasicClass, newBasicClass: BasicClass) {
        val basicClasses = _basicClassesLiveData.value ?: return
        _basicClassesLiveData.value = basicClasses.map { if (it == oldBasicClass) newBasicClass else it }
    }

    fun updateBasicClass(indexToUpdate: Int, newBasicClass: BasicClass) {
        val basicClasses = _basicClassesLiveData.value ?: return
        _basicClassesLiveData.value = basicClasses.mapIndexed { index, basicClass -> if (index == indexToUpdate) newBasicClass else basicClass }
    }
}