package fr.devid.demoapplication

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.basic_list_item.view.*


class BasicAdapter(var basicClasses: List<BasicClass>, private val removeCallback: (BasicClass) -> Unit) : RecyclerView.Adapter<BasicAdapter.BasicViewHolder>() {

    companion object {
        private var counter = 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BasicViewHolder {
        val counter = ++counter
        Log.d("BasicAdapter", "Creation d'un viewholder n° $counter")
        return BasicViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.basic_list_item, parent, false)).apply {
            itemView.tv_view_holder.text = "ViewHolder n° $counter"
            itemView.bt_delete.setOnClickListener {
                val position = adapterPosition
                if (position == RecyclerView.NO_POSITION) return@setOnClickListener
                removeCallback(basicClasses[position])
            }
        }
    }

    override fun getItemCount(): Int {
        return basicClasses.size
    }

    override fun onBindViewHolder(holder: BasicViewHolder, position: Int) {
        holder.bind(basicClasses[position])

    }

    class BasicViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(basicClass: BasicClass) {
            itemView.tv_number.text = "N° ${basicClass.number}"
            itemView.bt_delete.isVisible = basicClass.number % 2 == 1
        }

    }

}

data class BasicClass(val number: Int)