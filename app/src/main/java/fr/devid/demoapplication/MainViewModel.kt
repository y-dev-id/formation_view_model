package fr.devid.demoapplication

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*
import java.lang.Exception

class MainViewModel : ViewModel() {

    class Pays(val name: String)

    private val _countLiveData = MutableLiveData(0)
    val countLiveData: LiveData<Int> = _countLiveData

    private val _paysLiveData = MutableLiveData<List<Pays>>()
    val paysLiveData: LiveData<List<Pays>> = _paysLiveData

    init {
        getAllPays()
    }

    fun increment() {
        _countLiveData.value = (countLiveData.value ?: 0) + 1
    }

    private fun getAllPays() {
        // viewModelScope.launch(Dispatchers.MAIN) { correspond au MainThread
        // viewModelScope.launch(Dispatchers.IO) { correspond aux Input/Output (appel réseau, lecture/écriture de fichier disque, DB, etc.)
        // viewModelScope.launch(Dispatchers.DEFAULT) { correspond à des opérations utilisant le processeur
        viewModelScope.launch {
            println("getAllPays avant withContext ${Thread.currentThread().name}")
            try {
                _paysLiveData.value = withContext(Dispatchers.IO) {
                    println("getAllPays dans withContext ${Thread.currentThread().name}")
                    getAllPaysRetrofit()
                }
            } catch (ex: Exception) {
                Log.e("MainViewModel", "erreur lors de getPays", ex)
            }
            println("getAllPays après withContext ${Thread.currentThread().name}")
        }
    }

    private suspend fun getAllPaysRetrofit(): List<Pays> {
        delay(1000L)
        return listOf(Pays("France"))
    }

}