package fr.devid.demoapplication

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController

/**
 * A simple [Fragment] subclass.
 */
class SplashFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onStart() {
        super.onStart()
        Log.d("SplashFragment", "Navigation vers fragment login")
        val point = Point(1, 2, 9)
        findNavController().navigate(SplashFragmentDirections.actionSplashFragmentToLoginFragment(point))
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("SplashFragment", "onDestroy")
    }

}
