package fr.devid.demoapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_3.view.*

/**
 * A simple [Fragment] subclass.
 */
class Fragment3 : Fragment() {

    private val fragment3ViewModel: Fragment3ViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_3, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        val adapter = BasicAdapter(emptyList(), { basicClass ->
//            fragment3ViewModel.removeBasicClass(basicClass)
//        })
//        val adapter = BasicAdapter(emptyList(), fragment3ViewModel::removeBasicClass)
        val adapter = BasicListAdapter { basicClass ->
            fragment3ViewModel.removeBasicClass(basicClass)
        }
        view.rv_basic_classes.adapter = adapter

        view.bt_add.setOnClickListener {
            fragment3ViewModel.addBasicClass()
        }

        fragment3ViewModel.basicClassesLiveData.observe(viewLifecycleOwner, Observer { basicClasses ->
            adapter.submitList(basicClasses)
        })

    }

}
