package fr.devid.demoapplication

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Point(val x: Int, val y: Int, val z: Int) : Parcelable
