package fr.devid.demoapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class NavigationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)

//        println("Avant le thread ${Thread.currentThread().name}")
//        val testThread = object : Thread() {
//            override fun run() {
//                println("Dans le run ${name}")
//                Thread.sleep(1000L)
//                println("Après le sleep")
//            }
//        }
//        testThread.start()
//        testThread.join()
//        println("Après le thread ${Thread.currentThread().name}")

//        println("Avant le thread ${Thread.currentThread().name}")
//         GlobalScope.launch {
//            testCoroutine()
//        }
//        println("Après le thread ${Thread.currentThread().name}")
//
//        for (i in 0..1000) {
//            GlobalScope.launch {
//                println("n°$i ${Thread.currentThread().name}")
//                delay(1000)
//                println("n°$i bis ${Thread.currentThread().name}")
//            }
//        }
    }

    private suspend fun testCoroutine() {
        println("Dans le run ${Thread.currentThread().name}")
        delay(1000L)
        println("Après le sleep ${Thread.currentThread().name}")
    }
}
