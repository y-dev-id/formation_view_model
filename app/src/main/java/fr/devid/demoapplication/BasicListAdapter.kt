package fr.devid.demoapplication

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.basic_list_item.view.*


class BasicListAdapter(private val removeCallback: (BasicClass) -> Unit) : ListAdapter<BasicClass, BasicListAdapter.BasicListViewHolder>(BasicClassDiffUtil()) {

    companion object {
        private var counter = 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BasicListViewHolder {
        val counter = ++counter
        Log.d("BasicAdapter", "Creation d'un viewholder n° $counter")
        return BasicListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.basic_list_item, parent, false)).apply {
            itemView.tv_view_holder.text = "ViewHolder n° $counter"
            itemView.bt_delete.setOnClickListener {
                val position = adapterPosition
                if (position == RecyclerView.NO_POSITION) return@setOnClickListener
                removeCallback(getItem(position))
            }
        }
    }

    override fun onBindViewHolder(holder: BasicListViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class BasicListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(basicClass: BasicClass) {
            itemView.tv_number.text = "N° ${basicClass.number}"
            itemView.bt_delete.isVisible = basicClass.number % 2 == 1
        }

    }

}

internal class BasicClassDiffUtil : DiffUtil.ItemCallback<BasicClass>() {

    override fun areItemsTheSame(oldItem: BasicClass, newItem: BasicClass): Boolean {
        // Normalement ici on vérifie si l'id est identique uniquement
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: BasicClass, newItem: BasicClass): Boolean {
        // Ici on vérifie toutes les autres propriétés
        return oldItem == newItem
    }

}