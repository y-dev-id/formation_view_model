package fr.devid.demoapplication

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.navGraphViewModels
import fr.devid.demoapplication.databinding.Fragment2Binding
import kotlinx.android.synthetic.main.fragment_2.*

/**
 * A simple [Fragment] subclass.
 */
class Fragment2 : Fragment() {

    private val mainViewModel: MainViewModel by navGraphViewModels(R.id.main_nav_graph)


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
//        return Fragment2Binding.inflate(inflater, container, false).apply {
//            fragment2ViewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)
//            lifecycleOwner = viewLifecycleOwner
//        }.root
        val binding = Fragment2Binding.inflate(inflater, container, false)
        binding.fragment2ViewModel = mainViewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }
}
